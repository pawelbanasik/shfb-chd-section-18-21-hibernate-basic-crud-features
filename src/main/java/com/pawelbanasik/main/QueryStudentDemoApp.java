package com.pawelbanasik.main;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.pawelbanasik.domain.Student;

public class QueryStudentDemoApp {

	public static void main(String[] args) {

		SessionFactory factory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Student.class)
				.buildSessionFactory();

		Session session = factory.getCurrentSession();

		try {

			// start transaction
			session.beginTransaction();

			// query students
			List<Student> theStudents = session.createQuery("from Student").list();

			// display the students
			displayStudents(theStudents);

			// query students: lastName = 'Doe'
			theStudents = session.createQuery("from Student s where s.lastName='Doe'").list();

			// display the students
			System.out.println("\n\nStudents who have the last name of Doe.");
			displayStudents(theStudents);

			// query students: lastName='Doe' OR firstName='Daffy'
			theStudents = session.createQuery("from Student s where s.lastName='Doe' OR s.firstName='Daffy'").list();

			System.out.println("\n\nStudents who have the last name of Doe OR lastName of Daffy.");
			displayStudents(theStudents);

			// query students where email LIKE '%luv2code.com'
			theStudents = session.createQuery("from Student s where s.email LIKE '%luv2code.com'").list();
			System.out.println("\n\nStudents whose email ends int luv2code.com");
			displayStudents(theStudents);
			
			
			
			
			// commit transaction
			session.getTransaction().commit();
			System.out.println("Done!");

		} finally {
			factory.close();
		}

	}

	private static void displayStudents(List<Student> theStudents) {
		for (Student tempStudent : theStudents) {
			System.out.println(tempStudent);
		}
	}

}
